package main

import (
	"bitbucket.org/mattbaird/goose/lib/goose"
	"database/sql"
	"fmt"
	"log"
	"path/filepath"
	"time"
)

var statusCmd = &Command{
	Name:    "status",
	Usage:   "",
	Summary: "dump the migration status for the current DB",
	Help:    `status extended help here...`,
	Run:     statusRun,
}

type StatusData struct {
	Source string
	Status string
}

func statusRun(cmd *Command, args ...string) {
	conf, err := dbConfFromFlags()
	if err != nil {
		log.Fatal(err)
	}

	db, e := sql.Open(conf.Driver.Name, conf.Driver.OpenStr)
	if e != nil {
		log.Fatal("couldn't open DB:", e)
	}
	defer db.Close()

	// collect all migrations
	min := int64(0)
	max := int64((1 << 63) - 1)
	migrations, e := goose.CollectMigrations(db, conf.MigrationsDir, min, max, true)
	if e != nil {
		log.Fatal(e)
	}

	// set the default schema
	if conf.Driver.Name == "postgres" && len(conf.Driver.Schema) > 0 {
		_, err = db.Exec(`SET search_path = ` + conf.Driver.Schema)
		if err != nil {
			log.Fatal("error setting postgres schema:", e)
		}
	}

	// must ensure that the version table exists if we're running on a pristine DB
	if _, e := goose.EnsureDBVersion(conf, db); e != nil {
		log.Fatal("Failed to EnsureDBVersion:", e)
	}
	fmt.Printf("goose: status for environment '%v'\n", conf.Env)
	fmt.Println("    Applied At                  Migration")
	fmt.Println("    =======================================")
	for _, m := range migrations {
		printMigrationStatus(db, m.Version, filepath.Base(m.Source))
	}
}

func printMigrationStatus(db *sql.DB, version int64, script string) {
	var row goose.MigrationRecord
	q := fmt.Sprintf("SELECT tstamp, is_applied FROM goose_db_version WHERE version_id=%d ORDER BY tstamp DESC LIMIT 1", version)
	e := db.QueryRow(q).Scan(&row.TStamp, &row.IsApplied)

	if e != nil && e != sql.ErrNoRows {
		log.Fatal(e)
	}

	var appliedAt string

	if row.IsApplied {
		appliedAt = row.TStamp.Format(time.ANSIC)
	} else {
		appliedAt = "Pending"
	}

	fmt.Printf("    %-24s -- %v\n", appliedAt, script)
}
